// Randomize first slide

const randomSlide = () => {
  const slides = document.querySelectorAll('.carousel-item');
  const slideNum = Array.from(slides);
  const randomNum = Math.floor(Math.random() * slideNum.length) + 1;
  const randomNumIndex = randomNum - 1;
  let i = 0;
  for (i >= 0; i <= slideNum.length; i += 1) {
    if (i !== randomNumIndex) {
      slideNum[i].classList.remove('active');
    }
    slideNum[randomNumIndex].classList.add('active');
    return;
  }
};
window.onload = randomSlide();

const btnText = document.querySelectorAll('.read-more');

btnText.forEach((btn) => {
  btn.addEventListener('click', (e) => {
    const currentItem = e.currentTarget;
    const dots = currentItem.parentElement.children[2].firstElementChild;
    const moreText = currentItem.parentElement.children[2].children[1];
    const img = currentItem.parentElement.previousElementSibling;
    const innerText = currentItem.firstElementChild;
    if (dots.style.display === 'none') {
      dots.style.display = 'inline';
      innerText.textContent = 'Read more';
      moreText.style.display = 'none';
      img.style.display = 'flex';
    } else {
      dots.style.display = 'none';
      innerText.textContent = 'Read less';
      moreText.style.display = 'inline';
      img.style.display = 'none';
    }
  });
});
